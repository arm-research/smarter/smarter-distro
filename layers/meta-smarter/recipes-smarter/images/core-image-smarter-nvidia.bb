DESCRIPTION = "Nvidia Image with SMARTER, a cloud native edge foundation.  \
The image includes k3s and necessary support to run in edge environments."

COMPATIBLE_MACHINE = "(tegra)"

TEGRA_INITRD_INSTALL ??= ""
INITRD_FSTYPES ??= "${INITRAMFS_FSTYPES}"
TEGRA_INITRD_BASEUTILS ?= "busybox"

ROOTFSPART_SIZE = "5368709120"
PERSISTENT_SIZE = "10485760"

SERIAL_CONSOLES = "115200;ttyTHS0 115200;ttyTCU0"
KERNEL_ARGS = "console=ttyTCU0,115200 console=ttyTHS0,115200 OS=l4t fbcon=map:0"

IMAGE_INSTALL_append = "\
    tegra-tools \
    tegra-libraries \
    tegra-minimal-init \
    nvidia-docker \
    gstreamer1.0-omx-tegra \
    gstreamer1.0-plugins-nvvideo4linux2 \
    gstreamer1.0-plugins-nveglgles \
    gstreamer1.0-plugins-nvvideosinks \
    gstreamer1.0-plugins-tegra \
    tegra-mmapi \
    tensorrt \
    cudnn \
    cuda-command-line-tools \
    cuda-target-environment \
    cuda-driver \
    libvisionworks \
    tegra-redundant-boot \
    tegra-nvpmodel \
    ${TEGRA_INITRD_BASEUTILS} \
    ${ROOTFS_BOOTSTRAP_INSTALL} \
    ${TEGRA_INITRD_INSTALL} \
    tegra-bup-payload \
    nv-tegra-release \
    tegra-tools-tegrastats \
    "

LICENSE = "MIT"

require core-image-smarter.bb

TOOLCHAIN_HOST_TASK_append = " nativesdk-intltool nativesdk-glib-2.0"
TOOLCHAIN_HOST_TASK_remove_task-populate-sdk-ext = " nativesdk-intltool nativesdk-glib-2.0"
LICENSE_FLAGS_WHITELIST_append = " commercial_gstreamer1.0-omx-tegra"
