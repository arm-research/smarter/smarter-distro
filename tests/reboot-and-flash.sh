#!/bin/bash

[[ "$TRACE" ]] && set -ex

CURDIR=$(dirname $0)

MAX_TRIES=10
LAST_POD_RESULT=""
LAST_NODE_RESULT=""
JSON_LOG_OUTPUT=""
CONTINUE_TEST=1
EXIT_CODE=0

# Sets the nodename of the node to be used to test
USBID_TO_TEST="0956:7019"

# Namespace that both the nsp agent and the test pods will be using
NAMESPACE="default"

# Allow a longer timeout for when the image must be downloaded
LARGE_TIMEOUT=120 

# kubectl command
KUBECTL_CMD="kubectl"
KUBECTL_CONFIGFILE=${CURDIR}/kube.config

function logecho() {
        echo $(date +"%d/%m/%Y %H:%M:%S") $*
}

function check_required_vars() {
        if [ -z "${CI_KUBE_SERVER}" ]; then
                logecho "In order to deploy, CI_KUBE_SERVER variable must be set"
                (exit 1)
                return
        fi
        (exit 0)
        if [ -z "${CI_KUBE_TOKEN}" ]; then
                logecho "In order to deploy, CI_KUBE_TOKEN variable must be set"
                (exit 1)
                return
        fi
        if [ -z "${CI_SSH_TESTING_PRIVATE_KEY}" ]; then
                logecho "In order to deploy, CI_SSH_TESTING_PRIVATE_KEY variable must be set"
                (exit 1)
                return
        fi
        if [ -z "${CI_ROOT_TESTING_PASSWORD}" ]; then
                logecho "In order to deploy, CI_ROOT_TESTING_PASSWORD variable must be set"
                (exit 1)
                return
        fi
}

function check_for_USB_device() {
        USBID=$1

        EXISTING_USB=$(lsusb 2>/dev/null| grep ${USBID}) 
        if [ ! -z "${EXISTING_USB}" ]
        then
                (exit 0)
                return
        fi
        (exit 1)
        return
}

function wait_for_USB_device() {
        USBID=$1
        TIMEOUT=${2:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        SECS_WAIT=5
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                RETURN_CODE=0
                EXISTING_USB=$(lsusb 2>/dev/null| grep ${USBID}) &&
                if [ ! -z "${EXISTING_USB}" ]
                then
                        (exit 0)
                        return
                fi
                sleep ${SECS_WAIT}
        done
        logecho "Device ${USBID} did not appear after ${TIMEOUT}s, giving up"
        (exit 1)
        return
}

#******************************************************************************************
# Prepare execution
#******************************************************************************************

logecho "Started test"

# Check which host are we running on: Linux or Darwin, also are we crosscompiling?
case $(uname) in
        Linux)
                logecho "Running on Linux";
                PKILLCMD="pkill -KILL -s 0 ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE}"
                ;;
        Darwin)
                logecho "Running on MacOS";
                PKILLCMD="pkill -g 0 ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE}"
                ;;
        *)
                "I am lost here, running on $(uname) and I do not know them";
                exit 1
                ;;
esac

case $(uname -m) in
        x86_64)
                MACHINE_HOST=amd64;
                ;;
        armv7l)
                MACHINE_HOST=arm32v7;
                ;;
        aarch64)
                MACHINE_HOST=arm64v8;
                ;;
        *)
                logecho "Unrecognized archuitecture $(uname -m)";
                exit 1;;
esac

FLAG_CREATE_DELETE_NAMESPACE=1

while getopts N:U: name
do
        case $name in
        N)
            NODENAME_TO_TEST="$OPTARG";; 
        U)
            USBID_TO_TEST="$OPTARG";; 
        *)
            ;;
        esac
done

if [ -z "$NODENAME_TO_TEST" ]
then
        echo "Please set NODENAME_TO_TEST in your config.toml or environment"
        exit 1
fi

check_required_vars
if [ $? -gt 0 ]
then
        exit $?
fi

KUBE_SERVER_IP=$(echo ${CI_KUBE_SERVER} | sed -n -e "s/https:\/\/\([0-9.]*\):[0-9]*/\1/p")

if [ -z "${KUBE_SERVER_IP}" ]
then
        logecho "Could not extract the IP from the CI_KUBE_SERVER info, is it on the format https://[IP]:[PORT]"
        exit 1
fi

echo "${CI_SSH_TESTING_PRIVATE_KEY}" > ${CURDIR}/testing_private.key
chmod 600 ${CURDIR}/testing_private.key
SSH_TESTING_PUBLIC_KEY=$(ssh-keygen -y -f ${CURDIR}/testing_private.key)

exit 0

#******************************************************************************************
# Flashing initiated
#******************************************************************************************

logecho "Checking if the device is already on flashing mode"

check_for_USB_device "${USBID_TO_TEST}"

if [ $? -gt 0 ]
then
        logecho "Device is not on flashing mode so try putting it"

        ####
        # Reboot the machine
        ###

        logecho "Connecting to the testing HW ${NODENAME_TO_TEST} and rebooting into recovery mode"

        SSH_RESULT=$(ssh -i ${CURDIR}/testing_private.key -o StrictHostKeyChecking=no -o GlobalKnownHostsFile=/dev/null -o UserKnownHostsFile=/dev/null "root@${NODENAME_TO_TEST}.local" "/bin/bash -c 'nohup /usr/bin/reboot_python.py 10 >/dev/null 2>&1 &'")

        if [ $? -gt 0 ]
        then
                logecho "Failure to connect to the node and reboot it"
                logecho "Log of ssh: ${SSH_RESULT}"
                exit 1
        fi

        logecho "Testing HW ${NODENAME_TO_TEST} rebooting now, waiting for it to appear as flashable"

        wait_for_USB_device "${USBID_TO_TEST}"

        if [ $? -gt 0 ]
        then
                logecho "Device ${USBID_TO_TEST} not connected, bailing"
                exit 1
        fi
fi

logecho "Device ${USBID_TO_TEST} ready to flash"

mkdir ${CURDIR}/deploy
(
        PWD_DIR=$(pwd)
        cd ${CURDIR}/deploy
        unzip ${PWD_DIR}/poky/build/tmp/deploy/images/jetson-xavier/core-image-smarter-nvidia-jetson-xavier.tegraflash.zip
        ./smarter-doflash.sh --k3s-master ${KUBE_SERVER_IP} --k3s-token "${CI_KUBE_TOKEN}" --hostname "${NODENAME_TO_TEST}" --sshkey "${SSH_TESTING_PUBLIC_KEY}" --passwd "CI_ROOT_TESTING_PASSWORD}"

        if [ $? -eq 0 ]
        then
                exit 0
        fi

        exit 1
)
if [ $? -eq 0 ]
then
        logecho "Device ${USBID_TO_TEST} flashed successfully"
        exit 0
fi

logecho "Device ${USBID_TO_TEST} did not flash correctly, please check"
exit 1
