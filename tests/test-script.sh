#!/bin/bash

CURDIR=$(dirname $0)

. ${CURDIR}/common-functions.sh

MAX_TRIES=10
LAST_LOG_RESULT=""
JSON_LOG_OUTPUT=""
CONTINUE_TEST=1
EXIT_CODE=0
NOK=0
FULL_LOG_DIR="${CURDIR}/logs"

# Namespace that both the nsp agent and the test pods will be using
NAMESPACE="default"

# Allow a longer timeout for when the image must be downloaded
LARGE_TIMEOUT=120
# Allow a longer timeout for when the image must be downloaded
VERY_LARGE_TIMEOUT=240

# kubectl command
KUBECTL_CMD="kubectl"
KUBECTL_CONFIGFILE=${CURDIR}/kube.config

#******************************************************************************************
# Prepare execution
#******************************************************************************************

logecho "Started test"

# Check which host are we running on: Linux or Darwin, also are we crosscompiling?
case $(uname) in
        Linux)
                logecho "Running on Linux";
                PKILLCMD="pkill -KILL -s 0 ${KUBECTL_CMD}"
                ;;
        Darwin)
                logecho "Running on MacOS";
                PKILLCMD="pkill -g 0 ${KUBECTL_CMD}"
                ;;
        *)
                "I am lost here, running on $(uname) and I do not know them";
                exit 1
                ;;
esac

case $(uname -m) in
        x86_64)
                MACHINE_HOST=amd64;
                ;;
        armv7l)
                MACHINE_HOST=arm32v7;
                ;;
        aarch64)
                MACHINE_HOST=arm64v8;
                ;;
        *)
                logecho "Unrecognized archuitecture $(uname -m)";
                exit 1;;
esac

FLAG_CREATE_DELETE_NAMESPACE=1

while getopts CK:N:P:R:S:T:L: name
do
        case $name in
        C) 
            FLAG_CREATE_DELETE_NAMESPACE=0;;
        K)
            KUBECTL_CMD="$OPTARG";;
        N)
            NODENAME_TO_TEST="$OPTARG";; 
        T)
            IMAGE_TAG="$OPTARG";;
        R)
            IMAGE_REPOSITORY_NAME="$OPTARG";;
        P)
            PODNAME_TEST="$OPTARG";;
        S)
            NAMESPACE="$OPTARG";;
        L)
            FULL_LOG_DIR="$OPTARG";;
        *)
            ;;
        esac
done

if [ ! -d "${FULL_LOG_DIR}" ]
then
        mkdir -p "${FULL_LOG_DIR}"
fi

if [ "${NAMESPACE}" == "default" ]
then
        FLAG_CREATE_DELETE_NAMESPACE=0
fi

logecho "Setting up the environment, credentials, etc...."
if [ ! -z "${CI_KUBE_TOKEN}" ]
then
        check_required_vars CI_KUBE_SERVER CI_KUBE_TOKEN
else
        check_required_vars CI_KUBE_SERVER CI_KUBE_USERNAME CI_KUBE_PASSWORD
fi
if [ ${NOK} -gt 0 ]
then
        exit ${NOK}
fi

create_secret 
if [ ${NOK} -gt 0 ]
then
        logecho "create_secret failed, check log"
        exit ${NOK}
fi

kube_set_cred 
if [ ${NOK} -gt 0 ]
then
        logecho "kube_set_cred failed, check log"
        exit ${NOK}
fi

set_ci_context
if [ ${NOK} -gt 0 ]
then
        logecho "set_ci_context failed, check log"
        exit ${NOK}
fi

if [ -z "$NODENAME_TO_TEST" ]
then
        echo "Please set NODENAME_TO_TEST in your config.toml or environment"
        exit 1
fi

#******************************************************************************************
# Tests initiated
#******************************************************************************************

####
# Initiate tests - verify if node is available
###

if [ ${CONTINUE_TEST} -gt 0 ]
then
        logecho "Check if node ${NODENAME_TO_TEST} exists"

        check_if_node_exists_not_ready ${NODENAME_TO_TEST}

        if [ ${NOK} -eq 0 ]
        then
                logecho "Node ${NODENAME_TO_TEST} exists and notready check what to do "

                check_if_node_running ${NODENAME_TO_TEST}

                if [ ${NOK} -gt 0 ]
                then
                        if [ ${LAST_LOG_RESULT} == "WithoutCNI" ]
                        then
                                logecho "Node ${NODENAME_TO_TEST} does not have a CNI and we waited 120s, need to fix that"
                                exit 1
                        fi
                        if [ ${LAST_LOG_RESULT} == "NotConnected" ]
                        then
                                logecho "Node ${NODENAME_TO_TEST} not connected, delete it and wait"
                                # K3s uses a node passwd, if we reinstalled the node it does not have the node-passwd but keeps the hostname. This prevents the node to connect
                                # Removing the node, removes the passwd (using a server script or after 1.19) so the node will reassociate and be alive

                                delete_node ${NODENAME_TO_TEST}
                                if [ ${NOK} -gt 0 ]
                                then
                                        logecho "Deleting node returned error log: ${LAST_LOG_RESULT}"
                                        exit 1
                                fi
                                logecho "Waiting for the node ${NODENAME_TO_TEST} to appear"

                                check_if_node_running ${NODENAME_TO_TEST}

                                if [ ${NOK} -gt 0 ]
                                then
                                        logecho "Node ${NODENAME_TO_TEST} not available, error ${LAST_LOG_RESULT}"
                                        logecho "No additional tests will be performed since the node did not appear"
                                        exit ${EXIT_CODE}
                                fi

                        fi
                fi
        fi
        logecho "Node ${NODENAME_TO_TEST} is available"
fi


if [ ${CONTINUE_TEST} -gt 0 ]
then
    check_if_kata_runtime_class_exists
    if [ ${NOK} -gt 0 ]
    then
        logecho "Runtime class kata does not exist"
        logecho "Adding Runtime class kata"
        create_object_from_template "$(cat ${CURDIR}/kata_rc.yaml)"

        check_if_kata_runtime_class_exists
        if [ ${NOK} -gt 0 ]
        then
            logecho "Could not add Runtime class kata does not exist"
            EXIT_CODE=${NOK}
            exit ${EXIT_CODE}
        fi
    fi
    logecho "Runtime class kata exists"
fi

####
# Prepare namespace if namespace is not default
###

if [ ${CONTINUE_TEST} -gt 0 ]
then
        if [ ${FLAG_CREATE_DELETE_NAMESPACE} -gt 0 ]
        then
                logecho "Deleting namespace ${NAMESPACE} if it exists"
                delete_namespace_if_exists ${NAMESPACE}

                logecho "Creating namespace ${NAMESPACE}"

                create_object_from_template "apiVersion: v1
kind: Namespace
metadata:
  name: ${NAMESPACE}
  labels:
    name: ${NAMESPACE}"
                
                if [ ${NOK} -gt 0 ]
                then
                        logecho "Creating namespace ${NAMESPACE} failed log: ${LAST_LOG_RESULT}"
                        CONTINUE_TEST=0
                        EXIT_CODE=${NOK}
                else
                        logecho "Created namespace ${NAMESPACE}"
                fi
        fi
fi

####
# Insert slave testing pods - (slaves are the ones that only respond to requests)
###

if [ ${CONTINUE_TEST} -gt 0 ]
then
        logecho "Starting pod yocto-test-server at node ${NODENAME_TO_TEST}"

        # ------------------
        # Start Pod from template
        # ------------------

        create_object_from_template "$(cat ${CURDIR}/server.yaml.template)"

        if [ ${NOK} -gt 0 ]
        then
                EXIT_CODE=${NOK}
                CONTINUE_TEST=0
        else
                # ------------------
                # wait for the pod to start for ${LARGE_TIMEOUT}s 
                # ------------------

                logecho "Pod yocto-test-server created at node ${NODENAME_TO_TEST} waiting max ${LARGE_TIMEOUT}"
                check_if_pod_running yocto-test-server ${NAMESPACE} ${LARGE_TIMEOUT}
                if [ ${NOK} -gt 0 ]
                then
                        logecho "Pod yocto-test-server did not run, error ${LAST_LOG_RESULT}"
                        logecho "${JSON_LOG_OUTPUT}"
                        CONTINUE_TEST=0
                else
                        logecho "Pod yocto-test-server running at node ${NODENAME_TO_TEST}"
                fi
        fi
fi

####
# Insert slave testing pods - (slaves are the ones that only respond to requests)
###

if [ ${CONTINUE_TEST} -gt 0 ]
then
        logecho "Starting pod yocto-test-server-host at node ${NODENAME_TO_TEST}"

        # ------------------
        # Start Pod from template
        # ------------------

        create_object_from_template "$(cat ${CURDIR}/server-host.yaml.template)"

        if [ ${NOK} -gt 0 ]
        then
                EXIT_CODE=${NOK}
                CONTINUE_TEST=0
        else
                # ------------------
                # wait for the pod to start for ${LARGE_TIMEOUT}s 
                # ------------------

                logecho "Pod yocto-test-server-host created at node ${NODENAME_TO_TEST} waiting max ${LARGE_TIMEOUT}"
                check_if_pod_running yocto-test-server-host ${NAMESPACE} ${LARGE_TIMEOUT}
                if [ ${NOK} -gt 0 ]
                then
                        logecho "Pod yocto-test-server-host did not run, error ${LAST_LOG_RESULT}"
                        logecho "${JSON_LOG_OUTPUT}"
                        CONTINUE_TEST=0
                else
                        logecho "Pod yocto-test-server-host running at node ${NODENAME_TO_TEST}"
                fi
        fi
fi

####
# Insert client testing pods - (clients are the active testing pods)
###

if [ ${CONTINUE_TEST} -gt 0 ]
then
        logecho "Waiting for DNS update"
	sleep 45

        logecho "Starting pod yocto-test-client at node ${NODENAME_TO_TEST}"

        # ------------------
        # Start Pod from template
        # ------------------

        create_object_from_template "$(cat ${CURDIR}/client.yaml.template)"

        if [ ${NOK} -gt 0 ]
        then
                EXIT_CODE=${NOK}
                CONTINUE_TEST=0
        else
                # ------------------
                # wait for the pod to start for LARGE_TIMEOUT}
                # ------------------

                logecho "Pod yocto-test-client created at node ${NODENAME_TO_TEST} waiting max ${LARGE_TIMEOUT}"
                check_if_pod_ended yocto-test-client ${NAMESPACE} ${VERY_LARGE_TIMEOUT}
                if [ ${NOK} -gt 0 ]
                then
                        logecho "Pod yocto-test-client did not run, error ${LAST_LOG_RESULT}"
                        logecho "${JSON_LOG_OUTPUT}"
                        EXIT_CODE=${NOK}
                        CONTINUE_TEST=0
                else
                        logecho "Pod yocto-test-client completed OK at node ${NODENAME_TO_TEST}"
                fi
        fi
fi



####
# Insert kata testing pod
###

if [ ${CONTINUE_TEST} -gt 0 ]
then
        logecho "Waiting for DNS update"
	sleep 45

        logecho "Starting pod yocto-test-kata-client at node ${NODENAME_TO_TEST}"

        # ------------------
        # Start Pod from template
        # ------------------

        create_object_from_template "$(cat ${CURDIR}/client_kata.yaml.template)"

        if [ ${NOK} -gt 0 ]
        then
                EXIT_CODE=${NOK}
                CONTINUE_TEST=0
        else
                # ------------------
                # wait for the pod to start for LARGE_TIMEOUT}
                # ------------------

                logecho "Pod yocto-test-kata-client created at node ${NODENAME_TO_TEST} waiting max ${LARGE_TIMEOUT}"
                check_if_pod_ended yocto-test-kata-client ${NAMESPACE} ${VERY_LARGE_TIMEOUT}
                if [ ${NOK} -gt 0 ]
                then
                        logecho "Pod yocto-test-kata-client did not run, error ${LAST_LOG_RESULT}"
                        logecho "${JSON_LOG_OUTPUT}"
                        EXIT_CODE=${NOK}
                        CONTINUE_TEST=0
                else
                        logecho "Pod yocto-test-kata-client completed OK at node ${NODENAME_TO_TEST}"
                fi
        fi
fi





####
# Insert client host networking testing pods - (clients are the active testing pods)
###

if [ ${CONTINUE_TEST} -gt 0 ]
then

        logecho "Starting pod yocto-test-client-host at node ${NODENAME_TO_TEST}"

        # ------------------
        # Start Pod from template
        # ------------------

        create_object_from_template "$(cat ${CURDIR}/client-host-networking.yaml.template)"

        if [ ${NOK} -gt 0 ]
        then
                EXIT_CODE=${NOK}
                CONTINUE_TEST=0
        else
                # ------------------
                # wait for the pod to start for LARGE_TIMEOUT}
                # ------------------

                logecho "Pod yocto-test-client created at node ${NODENAME_TO_TEST} waiting max ${LARGE_TIMEOUT}"
                check_if_pod_ended yocto-test-client ${NAMESPACE} ${LARGE_TIMEOUT}
                if [ ${NOK} -gt 0 ]
                then
                        logecho "Pod yocto-test-client-host did not run, error ${LAST_LOG_RESULT}"
                        logecho "${JSON_LOG_OUTPUT}"
                        EXIT_CODE=${NOK}
                        CONTINUE_TEST=0
                else
                        logecho "Pod yocto-test-clist-host completed OK at node ${NODENAME_TO_TEST}"
                fi
        fi
fi

####
# Insert smarter-device-manager pod
###

if [ ${CONTINUE_TEST} -gt 0 ]
then

        logecho "Starting pod smarter-device-management-test at node ${NODENAME_TO_TEST}"
        # ------------------
        # Start Pod from template
        # ------------------

        create_object_from_template "$(cat ${CURDIR}/smarter-device-management-pod-k3s.yaml.template)"

        if [ ${NOK} -gt 0 ]
        then
                EXIT_CODE=${NOK}
                CONTINUE_TEST=0
        else
                # ------------------
                # wait for the pod to start for LARGE_TIMEOUT 
                # ------------------

                logecho "Pod smarter-device-management-test created at node ${NODENAME_TO_TEST} waiting max ${LARGE_TIMEOUT}"
                check_if_pod_running smarter-device-management-test ${NAMESPACE} ${LARGE_TIMEOUT}
                if [ ${NOK} -gt 0 ]
                then
                        logecho "Pod smarter-device-management-test did not run, error ${LAST_LOG_RESULT}"
                        logecho "${JSON_LOG_OUTPUT}"
                        EXIT_CODE=${NOK}
                        CONTINUE_TEST=0
                fi

                if [ ${CONTINUE_TEST} -gt 0 ]
                then
                        logecho "Pod smarter-device-management-test running at node ${NODENAME_TO_TEST} waiting 20s for the devices to appear"
                        sleep 20
                fi
        fi
fi


####
# Insert smarter-device-manager testing pods - (clients are the active testing pods)
###
#
#if [ ${CONTINUE_TEST} -gt 0 ]
#then
#
#        logecho "Starting pod smarter-device-management-client at node ${NODENAME_TO_TEST}"
#
#        # ------------------
#        # Start Pod from template
#        # ------------------
#
#        create_object_from_template "$(cat ${CURDIR}/client-alsa.yaml.template)"
#
#        if [ ${NOK} -gt 0 ]
#        then
#                EXIT_CODE=${NOK}
#                CONTINUE_TEST=0
#        else
#                # ------------------
#                # wait for the pod to start for LARGE_TIMEOUTs 
#                # ------------------
#
#                logecho "Pod smarter-device-management-client created at node ${NODENAME_TO_TEST} waiting max ${LARGE_TIMEOUT}"
#                check_if_pod_ended smarter-device-management-client  ${NAMESPACE} ${LARGE_TIMEOUT} 
#                if [ ${NOK} -gt 0 ]
#                then
#                        logecho "Pod smarter-device-management-client did not run, error ${LAST_LOG_RESULT}"
#                        logecho "${JSON_LOG_OUTPUT}"
#                        EXIT_CODE=${NOK}
#                        CONTINUE_TEST=0
#                else
#                        logecho "Pod smarter-device-management-client completed OK at node ${NODENAME_TO_TEST}"
#                fi
#        fi
#fi
#
####
# Insert triton-gpu testing pods - (clients are the active testing pods)
###

if [ ${CONTINUE_TEST} -gt 0 ]
then
	TRITON_TIMEOUT=1200 # Triton takes some time to download and run 16min on my network 

        logecho "Starting pod triton-gpu-test at node ${NODENAME_TO_TEST}"

        # ------------------
        # Start Pod from template
        # ------------------

        create_object_from_template "$(cat ${CURDIR}/triton.yaml.template)"

        if [ ${NOK} -gt 0 ]
        then
                EXIT_CODE=${NOK}
                CONTINUE_TEST=0
        else
                # ------------------
                # wait for the pod to start for LARGE_TIMEOUTs 
                # ------------------

                logecho "Pod triton-gpu-test created at node ${NODENAME_TO_TEST} waiting max ${TRITON_TIMEOUT}"
                check_if_pod_running triton-gpu-test ${NAMESPACE} ${TRITON_TIMEOUT}
                if [ ${NOK} -gt 0 ]
                then
                        logecho "Pod triton-gpu-test did not run, error ${LAST_LOG_RESULT}"
                        logecho "${JSON_LOG_OUTPUT}"
                        EXIT_CODE=${NOK}
                        CONTINUE_TEST=0
                else
                        sleep 40
                        logecho "Checking if this pod triton-gpu-test created at node ${NODENAME_TO_TEST} is sill running after running for 20s"
                        check_if_pod_running triton-gpu-test ${NAMESPACE} 20
                        if [ ${NOK} -gt 0 ]
                        then
                                logecho "Pod triton-gpu-test failed afer ruunning for a while, error ${LAST_LOG_RESULT}"
                                logecho "${JSON_LOG_OUTPUT}"
                                EXIT_CODE=${NOK}
                                CONTINUE_TEST=0
                        else
                                logecho "Pod triton-gpu-test running OK at node ${NODENAME_TO_TEST}"
                        fi
                fi
        fi
fi

####
# Cleaning the tests, remove the slave pods - (slaves are the ones that only respond to requests)
###

logecho "Removing triton-gpu-test pod"
delete_pod_if_exists triton-gpu-test ${NAMESPACE} ${LARGE_TIMEOUT} 
if [ ${NOK} -gt 0 ]
then
        logecho "Pod triton-gpu-test not clean correctly, error ${LAST_LOG_RESULT}"
        logecho "${JSON_LOG_OUTPUT}"
else
        logecho "Pod triton-gpu-test removed"
fi

logecho "Tests finished cleaning the leftovers"
logecho "Removing smarter-device-management-client pod"
delete_pod_if_exists smarter-device-management-client ${NAMESPACE} ${LARGE_TIMEOUT} 
if [ ${NOK} -gt 0 ]
then
        logecho "Pod smarter-device-management-client not clean correctly, error ${LAST_LOG_RESULT}"
        logecho "${JSON_LOG_OUTPUT}"
else
        logecho "Pod smarter-device-management-client removed"
fi

logecho "Removing smarter-device-management-test pod"
delete_pod_if_exists smarter-device-management-test ${NAMESPACE} ${LARGE_TIMEOUT} 
if [ ${NOK} -gt 0 ]
then
        logecho "Pod smarter-device-management-test not clean correctly, error ${LAST_LOG_RESULT}"
        logecho "${JSON_LOG_OUTPUT}"
else
        logecho "Pod smarter-device-management-test removed"
fi

logecho "Removing yocto-test-client-host pod"
delete_pod_if_exists yocto-test-client-host ${NAMESPACE} ${LARGE_TIMEOUT} 
if [ ${NOK} -gt 0 ]
then
        logecho "Pod yocto-test-client-host not clean correctly, error ${LAST_LOG_RESULT}"
        logecho "${JSON_LOG_OUTPUT}"
else
        logecho "Pod yocto-test-client-host removed"
fi

logecho "Removing yocto-test-client pod"
delete_pod_if_exists yocto-test-client ${NAMESPACE} ${LARGE_TIMEOUT} 
if [ ${NOK} -gt 0 ]
then
        logecho "Pod yocto-test-client not clean correctly, error ${LAST_LOG_RESULT}"
        logecho "${JSON_LOG_OUTPUT}"
else
        logecho "Pod yocto-test-client removed"
fi

logecho "Removing yocto-test-server pod"
delete_pod_if_exists yocto-test-server ${NAMESPACE} ${LARGE_TIMEOUT}
if [ ${NOK} -gt 0 ]
then
        logecho "Pod yocto-test-server not clean correctly, error ${LAST_LOG_RESULT}"
        logecho "${JSON_LOG_OUTPUT}"
else
        logecho "Pod  yocto-test-server removed"
fi

logecho "Removing yocto-test-server-host pod"
delete_pod_if_exists yocto-test-server-host ${NAMESPACE} ${LARGE_TIMEOUT}
if [ ${NOK} -gt 0 ]
then
        logecho "Pod yocto-test-server-host not clean correctly, error ${LAST_LOG_RESULT}"
        logecho "${JSON_LOG_OUTPUT}"
else
        logecho "Pod  yocto-test-server-host removed"
fi



logecho "Removing Runtime class kata"
delete_runtime_class_if_exists kata
if [ ${NOK} -gt 0 ]
then
        logecho "Runtime class kata not clean correctly, error ${LAST_LOG_RESULT}"
        logecho "${JSON_LOG_OUTPUT}"
else
    logecho "Runtime class kata removed"
fi


if [ ${FLAG_CREATE_DELETE_NAMESPACE} -gt 0 ]
then
        logecho "Removing ${NAMESPACE} namespace"
        delete_namespace_if_exists ${NAMESPACE}
        logecho "${NAMESPACE} namespace removed"
fi

if [ ${CONTINUE_TEST} -gt 0 ]
then
        logecho "Tests completed succesfully" 
else
        logecho "Tests failed please check log" 
fi

exit ${EXIT_CODE}
