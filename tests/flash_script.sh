#!/bin/bash

CURDIR=$(dirname $0)

. ${CURDIR}/common-functions.sh

MAX_TRIES=10
LAST_POD_RESULT=""
LAST_NODE_RESULT=""
JSON_LOG_OUTPUT=""
CONTINUE_TEST=1
EXIT_CODE=0

# Sets the nodename of the node to be used to test
USBID_TO_TEST="0955:7019"

# Namespace that both the nsp agent and the test pods will be using
NAMESPACE="default"

# Allow a longer timeout for when the image must be downloaded
LARGE_TIMEOUT=120 

# kubectl command
KUBECTL_CMD="kubectl"
KUBECTL_CONFIGFILE=${CURDIR}/kube.config

#******************************************************************************************
# Prepare execution
#******************************************************************************************

logecho "Started test"

# Check which host are we running on: Linux or Darwin, also are we crosscompiling?
case $(uname) in
        Linux)
                logecho "Running on Linux";
                PKILLCMD="pkill -KILL -s 0 ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE}"
                ;;
        Darwin)
                logecho "Running on MacOS";
                PKILLCMD="pkill -g 0 ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE}"
                ;;
        *)
                "I am lost here, running on $(uname) and I do not know them";
                exit 1
                ;;
esac

case $(uname -m) in
        x86_64)
                MACHINE_HOST=amd64;
                ;;
        armv7l)
                MACHINE_HOST=arm32v7;
                ;;
        aarch64)
                MACHINE_HOST=arm64v8;
                ;;
        *)
                logecho "Unrecognized archuitecture $(uname -m)";
                exit 1;;
esac

FLAG_CREATE_DELETE_NAMESPACE=1

while getopts CK:N:P:R:S:T: name
do
        case $name in
        C) 
            FLAG_CREATE_DELETE_NAMESPACE=0;;
        K)
            KUBECTL_CMD="$OPTARG";;
        N)
            NODENAME_TO_TEST="$OPTARG";; 
        T)
            IMAGE_TAG="$OPTARG";;
        R)
            IMAGE_REPOSITORY_NAME="$OPTARG";;
        P)
            PODNAME_TEST="$OPTARG";;
        S)
            NAMESPACE="$OPTARG";;
        *)
            ;;
        esac
done

if [ "${NAMESPACE}" == "default" ]
then
        FLAG_CREATE_DELETE_NAMESPACE=0
fi

check_required_vars CI_KUBE_SERVER CI_KUBE_TOKEN CI_PRIVATE_KEY CI_ROOT_TESTING_PASSWORD  SMARTER_YOCTO_DIR_TO_FLASH SMARTER_YOCTO_FILE

if [ $NOK -gt 0 ]
then
        exit $NOK
fi

KUBE_SERVER_IP=$(echo ${CI_KUBE_SERVER} | sed -e "s/[^:]*:\/\/\([0-9.]*\).*/\1/")
KUBE_SERVER_PORT=$(echo ${CI_KUBE_SERVER} | sed -e "s/[^:]*:[^:]*:\([0-9.]*\).*/\1/")

if [ -z "${KUBE_SERVER_IP}" ]
then
	logecho "No able to get the IP address from the CI_KUBE_SERVER ${CI_KUBE_SERVER}"
	exit 1
fi

#create_secret 
#kube_set_cred 
##set_ci_context
#create_secret

if [ -z "$NODENAME_TO_TEST" ]
then
        logecho "Please set NODENAME_TO_TEST in your config.toml or environment"
        exit 1
fi

#******************************************************************************************
# Flashing initiated
#******************************************************************************************


####
# Reboot the machine
###


if [ -e "${SMARTER_YOCTO_FILE}" ]
then
	if [ -d "${SMARTER_YOCTO_DIR_TO_FLASH}" ]
	then
		logecho "Existing installation on ${SMARTER_YOCTO_DIR_TO_FLASH}" 
		exit 1
	fi
	mkdir -p "${SMARTER_YOCTO_DIR_TO_FLASH}"

	logecho "Untaring the file ${SMARTER_YOCTO_FILE}" 
	(tar -C "${SMARTER_YOCTO_DIR_TO_FLASH}" -xvf "${SMARTER_YOCTO_FILE}")
else
	if [ ! -d "${SMARTER_YOCTO_DIR_TO_FLASH}" ]
	then
		logecho "There is no Existing installation on ${SMARTER_YOCTO_DIR_TO_FLASH}" 
		exit 1
	fi
fi

SSH_KEYFILE_PRIVATE=$(pwd)/temp.private.key
SSH_KEYFILE_PUBLIC=$(pwd)/temp.public.key

echo "${CI_PRIVATE_KEY}" > "${SSH_KEYFILE_PRIVATE}"
chmod 0600 "${SSH_KEYFILE_PRIVATE}"

ssh-keygen -y -f "${SSH_KEYFILE_PRIVATE}" > "${SSH_KEYFILE_PUBLIC}"

logecho "Checking if the device is already on flashing mode"

check_for_USB_device "${USBID_TO_TEST}"

if [ $NOK -gt 0 ]
then
        logecho "Device is not on flashing mode so try putting it"

        # check if it is alive (on the network)

        is_host_alive "${NODENAME_TO_TEST}.local"

        if [ $NOK -gt 0 ]
        then
               logecho "Host ${HOST} not connected, bailing"
               exit 1
        fi

        logecho "Connecting to the testing HW ${NODENAME_TO_TEST} and rebooting into recovery mode"

        SSH_RESULT=$(ssh -i "${SSH_KEYFILE_PRIVATE}" -o StrictHostKeyChecking=no -o GlobalKnownHostsFile=/dev/null -o UserKnownHostsFile=/dev/null "root@${NODENAME_TO_TEST}.local" "systemctl reboot --reboot-argument=forced-recovery")

        if [ $? -gt 0 ]
        then
                logecho "Failure to connect to the node and reboot it"
                logecho "Log of ssh: ${SSH_RESULT}"
                exit 1
        fi

        logecho "Testing HW ${NODENAME_TO_TEST} rebooting now, waiting for it to appear as flashable"

        wait_for_USB_device "${USBID_TO_TEST}"

        if [ $NOK -gt 0 ]
        then
                logecho "Device ${USBID_TO_TEST} not connected, bailing"
                exit 1
        fi
fi

logecho "Device ${USBID_TO_TEST} ready to flash"

#(cd ${SMARTER_YOCTO_DIR_TO_FLASH};./smarter-doflash.sh --k3s-master "${CI_KUBE_SERVER}" --k3s-token "${CI_KUBE_TOKEN}" --sshkeyfile "${SSH_KEYFILE_PUBLIC}" --hostname "${NODENAME_TO_TEST}" --passwd "${CI_ROOT_TESTING_PASSWORD}")
(cd ${SMARTER_YOCTO_DIR_TO_FLASH};./smarter-doflash.sh --k3s-master "${KUBE_SERVER_IP}" --k3s-port "${KUBE_SERVER_PORT}" --k3s-token "${CI_KUBE_TOKEN}" --sshkey "${SSH_KEYFILE_PUBLIC}" --hostname "${NODENAME_TO_TEST}" --passwd "${CI_ROOT_TESTING_PASSWORD}")

if [ $? -gt 0 ]
then
       logecho "Decvice ${HOST} did not flash correctly"
        exit 1
fi

is_host_alive ${NODENAME_TO_TEST}.local

if [ $NOK -gt 0 ]
then
       logecho "Host ${HOST} did not restore, bailing"
        exit 1
fi
