apiVersion: v1
kind: Pod
metadata:
  name: yocto-test-client
  namespace: NAMESPACE
spec:
  serviceAccountName: default
  automountServiceAccountToken: false
  dnsPolicy: ClusterFirstWithHostNet
  hostname: yocto-test-client
  nodeName: NODE_TO_TEST
  restartPolicy: Never
  containers:
  - name: yocto-test-client
    imagePullPolicy: IfNotPresent
    image: alpine
    command: ["/bin/ash"]
    args:
    - "-c"
    - |
      RESULT=$(ping -c 1 yocto-test-server 2>&1)
      if [ $? -gt 0 ]
      then
           echo "DNS lookup failed: output was ${RESULT}"
           exit 17
      fi
      RESULT=$(nc -w 20 yocto-test-server 2000 < /dev/null 2>&1)
      if [ $? -gt 0 ]
      then
           echo "nc command failed: output was ${RESULT}"
           exit 14
      fi
      if [ "${RESULT}" != "Hello world!" ]
      then
           echo "Response of the nc command was :${RESULT}: and not :Hello World!:"
           exit 10
      fi
      GATEWAY=$(ip route | sed -n  -e "/default/s/^.*via \([^ ]*\) .*/\1/p")
      RESULT=$(nc -w 20 ${GATEWAY} 2000 < /dev/null 2>&1)
      if [ $? -gt 0 ]
      then
           echo "nc to the gateway failed: output was ${RESULT}"
           exit 12
      fi
      if [ "${RESULT}" != "Hello world!" ]
      then
           echo "Response of the nc command was :${RESULT}: and not :Hello World!:"
           exit 11
      fi
      RESULT=$(nc -w 20 ${GATEWAY} 2001 < /dev/null 2>&1)
      if [ $? -gt 0 ]
      then
           echo "nc to the gateway port failed: output was ${RESULT}"
           exit 15
      fi
      if [ "${RESULT}" != "Hello world!" ]
      then
           echo "Response of the nc command was :${RESULT}: and not :Hello World!:"
           exit 16
      fi
      wget -T 30 www.arm.com
      if [ $? -gt 0 ]
      then
           echo "wget failed failed"
           exit  13
      fi
      exit 0
    resources:
      limits:
        cpu: 100m
        memory: 10Mi
      requests:
        cpu: 100m
        memory: 10Mi
  tolerations:
  - key: "smarter.type"
    operator: "Equal"
    value: "edge"
    effect: "NoSchedule"
  terminationGracePeriodSeconds: 10
