#!/bin/bash

[[ "$TRACE" ]] && set -ex

CURDIR=$(dirname $0)

MAX_TRIES=10
LAST_POD_RESULT=""
LAST_NODE_RESULT=""
JSON_LOG_OUTPUT=""
CONTINUE_TEST=1
EXIT_CODE=0

#******************************************************************************************
# Prepare execution
#******************************************************************************************

if [ $# -lt 1 ]
then
        echo "$0 <config file> [token file]"
        echo "If token is not provide, the user authentication will be taken from the config file"
        exit 1
fi

if [ ! -e "$1" ]
then
        echo "File $1 does not exists" 
        exit 1
fi

if [ -z "$NODENAME_TO_TEST" ]
then
        echo "Please set NODENAME_TO_TEST in your config.toml or environment"
        exit 1
fi

export CI_KUBE_SERVER=$(sed -n -e "s/^ *server: *\([^ ]*\) *$/\1/p" "$1")
export CI_KUBE_CERTIFICATE_AUTHORITY_DATA=$(sed -n -e "s/^ *certificate-authority-data: *\([^ ]*\) *$/\1/p" "$1")
export CI_KUBE_CLIENT_CERTIFICATE_DATA=$(sed -n -e "s/^ *client-certificate-data: *\([^ ]*\) *$/\1/p" "$1")
export CI_KUBE_CLIENT_KEY_DATA=$(sed -n -e "s/^ *client-key-data: *\([^ ]*\) *$/\1/p" "$1")
export CI_KUBE_USERNAME=$(sed -n -e "s/^ *username: *\([^ ]*\) *$/\1/p" "$1")
export CI_KUBE_PASSWORD=$(sed -n -e "s/^ *password: *\([^ ]*\) *$/\1/p" "$1")
if [ $# -ge 2 ]
then
        export CI_KUBE_TOKEN=$(cat "$2")
fi
if [ -z "${CI_KUBE_CLIENT_CERTIFICATE_DATA}" ]
then
        unset CI_KUBE_CLIENT_CERTIFICATE_DATA
fi
if [ -z "${CI_KUBE_CLIENT_KEY_DATA}" ]
then
        unset CI_KUBE_CLIENT_KEY_DATA
fi
if [ -z "${CI_KUBE_USERNAME}" ]
then
        unset CI_KUBE_USERNAME
fi
if [ -z "${CI_KUBE_PASSWORD}" ]
then
        unset CI_KUBE_PASSWORD
fi

./test-script.sh -N ${NODENAME_TO_TEST} -S test
