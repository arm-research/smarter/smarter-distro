#!/bin/bash

#******************************************************************************************
# Prepare execution
#******************************************************************************************

if [ $# -lt 6 ]
then
        echo "A location with a working kube.config, token, private key file, root password, file to flash and the directory to use as temp as to be given, provided $# parameters"
        exit 1
fi

if [ ! -e "$1" -o ! -e "$2" -o ! -e "$3" ]
then
        echo "File $1, $2, $3 does not exist"
        exit 1
fi

if [ ! -e "$5" -a ! -d "$6" ]
then
        echo "Flash file $5 does not exist and flash directory $6 not populated"
        exit 1
fi

if [ -z "$NODENAME_TO_TEST" ]
then
        echo "Please set NODENAME_TO_TEST in your config.toml or environment"
        exit 1
fi

KUBECONFIG=$(cat $1)


export TRACE=1
export CI_KUBE_SERVER=$(echo "${KUBECONFIG}" | sed -n -e "s/^ *server: *\([^ ]*\) *$/\1/p")
export CI_KUBE_CERTIFICATE_AUTHORITY_DATA=$(echo "${KUBECONFIG}" | sed -n -e "s/^ *certificate-authority-data: *\([^ ]*\) *$/\1/p")
export CI_KUBE_USERNAME=$(echo "${KUBECONFIG}" | sed -n -e "s/^ *username: *\([^ ]*\) *$/\1/p")
export CI_KUBE_PASSWORD=$(echo "${KUBECONFIG}" | sed -n -e "s/^ *password: *\([^ ]*\) *$/\1/p")
export CI_KUBE_TOKEN=$(cat $2)
export CI_PRIVATE_KEY=$(cat $3)
export CI_ROOT_TESTING_PASSWORD="$4"
export SMARTER_YOCTO_FILE="$5"
export SMARTER_YOCTO_DIR_TO_FLASH="$6"

./flash_script.sh -N ${NODENAME_TO_TEST} -S test
